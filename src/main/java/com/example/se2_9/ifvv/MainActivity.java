package com.example.se2_9.ifvv;

import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.VideoView;
import android.content.pm.ActivityInfo;

public class MainActivity extends AppCompatActivity {
    Uri url;
    public static final  String FIRST_SHARED_FILE="com.example.user.parkinglot.myFirstPersistentObjectName";
    SharedPreferences settings;
    SharedPreferences.Editor myEditor;
    private static final String STATE_COUNTER = "counter";
    private static final String STATE_POINTER = "pointer";
    VideoView myVideo;
    private int pointer = 0;
    private int counter;
    private String[] viewArray = {
            "/stream?src=fragments/fragment0"
            , "/stream?src=cameras/View1"
            , "/stream?src=cameras/View2"
            , "/stream?src=cameras/View3"
            , "/stream?src=cameras/View4"
            , "/stream?src=cameras/View5"
            , "/stream?src=cameras/View6"
    };
    EditText baseURL;
    String URL1;
    Button cameraPlay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        settings=getSharedPreferences(FIRST_SHARED_FILE, 0);
        myEditor=settings.edit();
        myVideo= (VideoView)findViewById(R.id.videoView);
        URL1 ="http://ifvv.herokuapp.com";
        baseURL = findViewById(R.id.baseURL);
        baseURL.setText(URL1);
        baseURL.setHint(URL1);
        cameraPlay=findViewById(R.id.cameraPlay);
        pointer = settings.getInt(getString(R.string.pointer),0);
        counter = settings.getInt(getString(R.string.counter),0);
        cameraPlay.setText(Integer.toString(pointer));
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        playVideo();

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        onSave();

    }

    private void onSave() {
        myEditor.putInt(getString(R.string.pointer),pointer);
        myEditor.putInt(getString(R.string.counter),counter);
    }

    public  void prev(View v){
        pointer--;
        if(pointer<0) pointer=6;
        cameraPlay.setText(Integer.toString(pointer));
    }

    public  void next(View v){
        pointer++;
        if(pointer>6) pointer=0;
        cameraPlay.setText(Integer.toString(pointer));
    }
    public  void play(View v){
        playVideo();

    }

    private void playVideo() {
        url= Uri.parse(baseURL.getText().toString()+viewArray[pointer]);
        myVideo.setVideoURI(url);
        myVideo.setMediaController(new MediaController(this));
        myVideo.requestFocus();
        myVideo.seekTo(counter);
        myVideo.start();
    }

    @Override
    protected void onResume() {
        url= Uri.parse(baseURL.getText().toString()+viewArray[pointer]);
        myVideo.setVideoURI(url);
        myVideo.seekTo(counter);
        myVideo.resume();
        super.onResume();
    }

    @Override
    protected void onPause() {
        onSave();
        myVideo.suspend();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        myVideo.stopPlayback();
        super.onDestroy();
    }
}
